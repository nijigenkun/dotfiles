# Personal dotfiles
Made for use with gnu stow. To use, just use:

`stow <dir>`

If there's an error, backup and delete existing conflicting files.
